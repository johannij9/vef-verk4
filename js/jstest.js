var array = [
	"2015-11-07T20:00:00",
	"2015-11-14T20:00:00",
	"2015-11-12T20:00:00"
];

array.sort(function(a, b){
	var dateA = new Date(a);
	var dateB = new Date(b);

	return dateA - dateB;
});